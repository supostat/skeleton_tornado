# --- coding: utf8 ---
__author__ = 'vadim7j7'
# ----------------------------------------------------------------------------------------------------------------------
from tornado.web import RequestHandler, HTTPError
from jinja2.exceptions import TemplateNotFound


# ----------------------------------------------------------------------------------------------------------------------
class BaseHandler(RequestHandler):
    def data_received(self, chunk):
        pass

    @property
    def db(self):
        return self.db

    @property
    def env(self):
        return self.application.env

    def write_error(self, status_code, **kwargs):
        self.render('errors/%s.html' % status_code, code=status_code)

    def render(self, template_name, **kwargs):
        try:
            template = self.env.get_template(template_name)
        except TemplateNotFound:
            raise HTTPError(404)

        kwargs.update({
            'static_url': self.static_url,
            'xsrf_form_html': self.xsrf_form_html})

        self.write(template.render(kwargs))


# ----------------------------------------------------------------------------------------------------------------------
